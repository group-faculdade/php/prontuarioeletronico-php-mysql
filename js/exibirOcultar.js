function exibir_ocultar(){
    var valor = $("#tipo_pessoa").val();

    if(valor == "paciente"){
         $("#crm").hide();
         $("#sus").show();
         $("#prontuario").show();
     } else if(valor =="medico"){
        $("#sus").hide();
        $("#crm").show();
        $("#prontuario").hide();
     } else{
        $("#sus").hide();
        $("#crm").hide();
        $("#prontuario").hide();
     }
};