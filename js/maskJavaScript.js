function maskCPF() {
	var cpf = document.getElementById("cpf");
	var tam = cpf.value.length;

	if(tam == 3 || tam == 7){
		cpf.value += ".";
	} else
	if (tam == 11) {
		cpf.value += "-";
	}
}

function maskDATA() {
	var data = document.getElementById("data");
	var tam = data.value.length;

	if(tam == 2 || tam == 5){
		data.value += "/";
	} 
}

function maskCEP() {
	var cep = document.getElementById("cep");
	var tam = cep.value.length;

	if(tam == 2){
		cep.value += ".";
	} else
	if (tam == 6) {
		cep.value += "-";
	}
}

function verificarCampo(){
	var cpf = document.getElementById("cpf");
	var tamCpf = cpf.value.length;

	var cep = document.getElementById("cep");
	var tamCep = cep.value.length;
	
	var data = document.getElementById("data");
	var tamData = data.value.length;	

	if (tamCpf < 14) {
		cpf.style.borderColor = "red";
	}

	if (tamCep < 14) {
		cep.style.borderColor = "red";
	}

	if (tamData < 14) {
		data.style.borderColor = "red";
	}
}

//this passa o campo todo