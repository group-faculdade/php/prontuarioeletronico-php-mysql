<?php
	include "connection.php";
	session_start();
	if (!isset($_SESSION['user'])) 
		header("location:../index.php"); 
?>
<!DOCTYPE html>
	<html lang="en">
		<head>
			<title>Prontuário Eletrônico do Paciente</title>
			<meta charset="utf-8"/>
			<meta name="viewport" content="width=device-width, initial-scale=1"/>
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
			<link rel="stylesheet" type="text/css" href="../css/style.css"/>
			<link rel="icon" href="../img/prontuario.png" type="image/png"/>
		</head>
				
		<body>			
		<!--_____________________________NAV_____________________________-->	
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
				    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>                        
				    </button>
				</div>
				<div class="collapse navbar-collapse" id="myNavbar">
	      			<ul class="nav navbar-nav">
	        			<li class="active"><a href="../php/home.php"></span>Home</a></li>
	    	        	<li class="active"><a href="../php/consultas.php"></span>Consultas</a></li>
	        			<li class="active"><a href="../emManutencao.html"></span>Laudos</a></li>
	        			<li class="active"><a href="../emManutencao.html"></span>Laboratório</a></li>
	        			<li class="active"><a href="../php/cadastro.php"></span>Cadastro</a></li>
	        			<li class="active"><a href="../emManutencao.html"></span>Emergência</a></li>
	      			</ul>
				<div class="collapse navbar-collapse" id="myNavbar">
				    <ul class="nav navbar-nav navbar-right">
				        <li><a href="logout.php"><span class="glyphicon glyphicon-user"></span> Encerrar sessão</a></li>
				    </ul>
				</div>
			</div>
		</nav>
		<!--___________________________TOPO_____________________________-->	
		<div class="jumbotron">
			<img src="../img/03-2.jpg" class="img-responsive" style="width:100%" alt="Image">       
		</div>
				
		<!--_____________________________CONTEÚDO_____________________________-->
		
		<div class="container text-center">  		    
				<div class="row">
					<div class="col-sm-4">
						<img src="../img/Prontuario-eletronico.png" class="img-responsive" style="display: block;margin-left: auto;
		    margin-right: auto; width: 35%" alt="Image"/><br/>
					    <p>Maior integração e gerenciamento do cuidado</p> </a>
					</div>
					<div class="col-sm-4">
						<img src="../img/pep_pront.png" class="img-responsive" style="display: block;margin-left: auto;
		    margin-right: auto; width: 50%" alt="Image"/><br/>
		    			<p>Acesso remoto e simultâneo</p>
					</div>
					<div class="col-sm-4"> 
						<img src="../img/pep.png" class="img-responsive" style="display: block;margin-left: auto;
		    margin-right: auto; width: 35%" alt="Image"/><br/>
					    <p>Confidencialidade dos dados do paciente</p> </a>   
					</div>
				</div>
		</div><br/>
		
		<nav class = "menu">
	<a  href="../php/consultas.php">Consultas</a>
	<a href="../emManutencao.html">Exames</a>
	<a href="../emManutencao.html">Laboratório</a>
	<a href="../php/cadastro.php">Cadastro</a>
	<a href="../emManutencao.html">Emergência</a>
	<div class="animation start-home"></div>
</nav>
		
		
		<!--_____________________________FOOTER_____________________________-->	
		<footer class="container-fluid text-center">
			 <p>P E P<sup>©</sup></p>
		</footer>
	</body>
</html>