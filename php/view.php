<!DOCTYPE html>
<html lang="en">
	<head>
	 	<title>Consultas</title>
	  	<meta charset="utf-8">
	  	<meta name="viewport" content="width=device-width, initial-scale=1">
	  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	  	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>
	  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	  	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
	  	<link rel="stylesheet" type="text/css" href="../css/style.css">
	  	<link rel="icon" href="../img/prontuario.png" type="image/png"/>
	  	<script  src="../js/maskJavaScript.js"></script>
	</head>

	<body>
	<!--______________________MODAL__________________-->
	<div class="modal-content animate" tabindex="-1" role="dialog" id="delete-modal">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Confirmar exclusão</h4>
	      </div>
	      <div class="modal-body">
	        <p>Deseja excluir esta consulta?</p>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
	        <button type="button" class="btn btn-primary">Sim</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<!--_______________________NAV____________________-->
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
				    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>                        
				    </button>
				</div>
				<div class="collapse navbar-collapse" id="myNavbar">
	      			<ul class="nav navbar-nav">
	        			<li class="active"><a href="../php/home.php"></span>Home</a></li>
	      			</ul>
	      			<ul class="nav navbar-nav navbar-right">
				        <li><a href="../index.html"><span class="glyphicon glyphicon-user"></span> Encerrar sessão</a></li>
				    </ul>
				</div>
	<!--____________________CONTEÚDO__________________-->
		<div id="main" class="container-fluid">
			<h3 class="page-header">Visualizar</h3>
			<div class="row">
			 	<div class="col-md-4">
			 	 	<p><strong>Nome do Campo</strong></p>
			 	 	<p>{Valor do Campo}</p>
			 	</div> 
			</div>
			<hr />
			<div id="actions" class="row">
				<div class="col-md-12">
			  		<a href="edit.html" class="btn btn-default" >Editar</a>
			  		<a href="index.html" class="btn btn-default">Fechar</a>
			  		<a href="#" class="btn btn-default" data-toggle="modal" data-target="#delete-modal">Excluir</a>
			 	</div>
			</div>
		</div>

		
		</div>
		<div style="height: 50px; background-color:#fff"></div>
	<!--___________________FOOTER________________________-->
		<footer class="container-fluid text-center">
			<p>P E P<sup>©</sup></p>
		</footer>
	</body>
</html>