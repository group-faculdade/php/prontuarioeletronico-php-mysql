<?php
	include "connection.php";
	session_start();
	if (!isset($_SESSION['user'])) 
		header("location:../index.php"); 
?>
<!DOCTYPE html>
<html lang="en">
	<head>
	 	<title>Cadastro</title>
	  	<meta charset="utf-8">
	  	<meta name="viewport" content="width=device-width, initial-scale=1">
	  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	  	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
	  	<link rel="stylesheet" type="text/css" href="../css/style.css">
	  	<link rel="icon" href="../img/prontuario.png" type="image/png"/>
	  	<script  src="../js/maskJavaScript.js"></script>
	  	<script  src="../js/exibirOcultar.js"></script>
	  	<script src="../js/parsley.min.js"></script>
	</head>

	<body style="background-color: #62CFEA;">
	<!--_______________________NAV____________________-->
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
				    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>                        
				    </button>
				</div>
				<div class="collapse navbar-collapse" id="myNavbar">
	      			<div class="collapse navbar-collapse" id="myNavbar">
	      			<ul class="nav navbar-nav">
	        			<li class="active"><a href="../php/home.php"></span>Home</a></li>
	    	        	<li class="active"><a href="../php/consultas.php"></span>Consultas</a></li>
	        			<li class="active"><a href="../emManutencao.html"></span>Laudos</a></li>
	        			<li class="active"><a href="../emManutencao.html"></span>Laboratório</a></li>
	        			<li class="active"><a href="../php/cadastro.php"></span>Cadastro</a></li>
	        			<li class="active"><a href="../emManutencao.html"></span>Emergência</a></li>
	      			</ul>
	      			<ul class="nav navbar-nav navbar-right">
				        <li><a href="../index.php"><span class="glyphicon glyphicon-user"></span> Encerrar sessão</a></li>
				    </ul>
				<div class="collapse navbar-collapse" id="myNavbar">
				    <ul class="nav navbar-nav navbar-right">
				     
				    </ul>
				</div>
			</div>
		</nav>
	<!--_____________________________CONTEÚDO___________________________-->
	<div class="container text-center" id="form-text">
		<h5 id="texto">Preencha o seguinte formulário:</h5>
	</div> <br/> 

	<div class="bs-callout bs-callout-warning hidden">
	  <h4>Erro!</h4>
	  <p>Esse formulário não pode ser validado:(</p>
	</div>

	<script type="text/javascript">
		<script type="text/javascript">
		$(function () {
		  $('#demo-form').parsley().on('field:validated', function() {
		    var ok = $('.parsley-error').length === 0;
		    $('.bs-callout-info').toggleClass('hidden', !ok);
		    $('.bs-callout-warning').toggleClass('hidden', ok);
		  })
		  .on('form:submit', function() {
		    return false; // Don't submit form for this demo
		  });
		});
		</script>
	</script>
	
	<!--___________________________FORMULÁRIO___________________________-->
		<div id="formulario" class="container">
			<form method="POST" action=""  name="demo-form" data-parsley-validate="">
			  
			  <div class="form-row">
			   	<div class="form-group col-md-4">
			      	<label><b>Nome:</b></label>
			      	<input type="text" class="form-control" placeholder="Nome" name="nome" required="">
			    </div>
			    <div class="form-group col-md-4">
			      	<label><b>CPF:</b></label>
			      	<input type="text" class="form-control" id="cpf" onkeypress="$(this).mask('000.000.000-00');"  placeholder="000.000.000-00" name="cpf" data-parsley-trigger="change" required="" data-parsley-length="[14, 14]"  data-parsley-required data-parsley-error-message="Verifique o número de caracteres do campo" > 
			    </div>
			  	<div class="form-group col-md-4">
			    	<label for="inputAddress2"><b>RG:</b></label>
			    	<input type="text" class="form-control" id="rg" onkeypress="$(this).mask('000.000.000');" placeholder="000.000.000" name="rg" data-parsley-trigger="change" required="" data-parsley-length="[11, 11]" data-parsley-required data-parsley-error-message="Verifique o número de caracteres do campo">
			  	</div>
			 </div>

			 <div class="form-row">
			 	<div class="form-group col-md-2">
			 	  	<label><b>Data de nascimento:</b></label>
			 	  	<input type="date" class="form-control" id="dt_nasc" onkeypress="maskDATA()" maxlength="10" placeholder="00/00/0000" name="dt_nasc" data-parsley-trigger="change" required="" data-parsley-length="[10, 10]" data-parsley-required data-parsley-error-message="Verifique o número de caracteres do campo">
			 	</div>
			    <div class="form-group col-md-2">
			      	<label><b>Sexo:</b></label>
			      	<select name="sexo[]" class="form-control">
				      	<option selected>-</option>
				      	<option value="M">M</option>
				      	<option value="F">F</option>
			      	</select>
			    </div>
			    <div class="form-group col-md-4">
			      <label><b>Email:</b></label>
			  	  <input type="email" class="form-control" placeholder="email_@email.com" name="email" data-parsley-trigger="change" required="">
			    </div>
			    <div class="form-group col-md-4">
			      <label><b>Telefone:</b></label>
			  	  <input type="text" class="form-control" onkeypress="$(this).mask('(00) 0000-0000');" placeholder="(00) 00000-0000" name="tel" data-parsley-trigger="change" >
			    </div>
			</div>
			
			<div style="height: 10px; background-color:#fff"></div>
			
			<div class="form-row">
			    <div class="form-group col-md-4">
			      <label><b>Município:</b></label>
			  	  <input type="text" class="form-control" placeholder="Município" name="municipio" data-parsley-trigger="change" required="">
			    </div>
			    <div class="form-group col-md-4">
			      <label><b>CEP:</b></label>
			  	  <input type="text" class="form-control" id="cep" onkeypress="$(this).mask('00000-000');" placeholder="00000-000" name="cep" data-parsley-trigger="change" required="" data-parsley-length="[9, 9]" data-parsley-required data-parsley-error-message="Verifique o número de caracteres do campo">
			    </div>
			    <div class="form-group col-md-4">
			      <label><b>Complemento:</b></label>
			  	  <input type="text" class="form-control" placeholder="casa, apartamento, etc." name="complemento" data-parsley-trigger="change" required="">
			    </div>
			</div>

			<div style="height: 10px; background-color:#fff"></div>

			<div class="form-row">
				<div class="form-group col-md-4">
				    <label><b>Bairro:</b></label>
				    <input type="text" class="form-control" placeholder="Bairro" name="bairro" data-parsley-trigger="change" required=""> 
				</div>
				<div class="form-group col-md-4">
				 	<label><b>Rua:</b></label>
				 	<input type="text" class="form-control" placeholder="Rua" name="rua" data-parsley-trigger="change" >
				</div>
				<div class="form-group col-md-2">
				    <label><b>Estado:</b></label>
					<select class="form-control" name="uf[]">
				     	<option selected>-</option>
							<?php
							$queryUF = mysqli_query($connection, "SELECT * FROM estado ORDER BY uf ASC");
							while($linhas=mysqli_fetch_assoc($queryUF)){
								$uf = $linhas['uf'];
								echo "<option value='$uf'>$uf</option>";						
							}							
						?>
			      </select>
				</div> 
				<div class="form-group col-md-2">
			      <label><b>Selecione:</b></label>
			      <select class="form-control" id="tipo_pessoa" onchange="exibir_ocultar()">
				      <option selected>-</option>
				      <option value="medico">Médico</option>
				      <option value="paciente">Paciente</option>
			      </select>
			    </div>
			</div>

			<div style="height: 10px; background-color:#fff"></div>

			<div class="form-row">
			    <div class="form-group col-md-6">

			      	<div id="crm" style="display: none;">
			        	<label><b>CRM:</b></label>
			        	<input type="text" class="form-control" onkeypress="$(this).mask('0000000/00');" placeholder="0000000/00" name="crm" data-parsley-trigger="change" >
			     	</div>

			     	<div  id="sus" style="display: none;">
			         	<label><b>SUS:</b></label>
			         	<input type="text" class="form-control" onkeypress="$(this).mask('000 0000 0000 0000');" placeholder="000 0000 0000 0000" name="sus" data-parsley-trigger="change" >
			      	</div>

					<div  id="prontuario" style="display: none;">
			         	<label><b>Prontuario:</b></label>
			         	<input type="text" class="form-control" onkeypress="$(this).mask('000 0000 0000 0000');" placeholder="000 0000 0000 0000" name="prontuario" data-parsley-trigger="change" >
			      	</div>

			    </div> 
			  </div>					
			    
			 
			  	<div id="actions" class="row">
						<div class="col-md-12">
							<button  value="validate" type="submit" class="btn btn-primary" style="background-color: #62CFEA; border-color: #62CFEA; margin-left: 15px;">Salvar</button>
							<a href="index.php" class="btn btn-default">Cancelar</a>
			  		</div>
			  	</div>
			</form>


			<?php	
			$nmResolv = isset($_POST['nome']) ? $_POST['nome'] : ''; //remove erro da primeira vez que abre a presente página
			if(!empty(($nmResolv)and($_POST['cpf'])and($_POST['dt_nasc'])and($_POST['rg'])and($_POST['email'])and($_POST['tel'])
			and($_POST['municipio'])and($_POST['cep'])and($_POST['complemento'])and($_POST['bairro']))){
				
				$nome = $_POST['nome'];
				$cpf = $_POST['cpf'];
				$dt_nasc = $_POST['dt_nasc'];
				$rg = $_POST['rg'];
				$email = $_POST['email'];
				$tel = $_POST['tel'];
				$municipio = $_POST['municipio'];
				$cep = $_POST['cep'];
				$complemento = $_POST['complemento'];
				$bairro = $_POST['bairro'];
				$estado = implode(',', $_POST['uf']);
				$crm = $_POST['crm'];
				$prontuario = $_POST['prontuario'];
				$sus = $_POST['sus'];	
				$sexo = implode(',',$_POST['sexo']);
				$rua = $_POST['rua'];

				$query_dado2 = mysqli_query($connection, "SELECT id_uf FROM estado WHERE uf='$estado'");
				$dado_uf = mysqli_fetch_assoc($query_dado2);
				$fk_estado = $dado_uf['id_uf'];

				$query1 = "INSERT INTO Pessoa(nome, dt_nasc, cpf, rg, sexo, email, num_tel, rua, bairro, cidade, id_uf) VALUES ('$nome','$dt_nasc','$cpf','$rg','$sexo','$email','$tel','$rua','$bairro','$municipio','$fk_estado')";
				$insert1 = mysqli_query($connection, $query1);

				$rs = mysqli_query($connection, "SELECT * FROM Pessoa WHERE nome='$nome'");
				$dado_pessoa = mysqli_fetch_assoc($rs);
				$fk_pessoa = $dado_pessoa['id_pessoa'];

				if(!empty($_POST['crm'])){
					$query_m = "INSERT INTO medico(id_pessoa, crm) VALUES ('$fk_pessoa', '$crm')";
					$insert_m = mysqli_query($connection, $query_m);
				}else{					
					$query_pront = "INSERT INTO prontuario(num_prontuario) VALUES('$prontuario')";
					$insert_pront = mysqli_query($connection, $query_pront);

					$rs2 = mysqli_query($connection, "SELECT * FROM prontuario WHERE num_prontuario='$prontuario' LIMIT 1");
					$dado_pront = mysqli_fetch_assoc($rs2);
					$fk_prontuario = $dado_pront['id_prontuario'];

					$query_pa = "INSERT INTO paciente(id_pessoa, id_prontuario, num_sus) VALUES ('$fk_pessoa', '$fk_prontuario', '$sus')";
					$insert_pa = mysqli_query($connection, $query_pa);
				}	
				mysqli_close($connection);
			}
			
		?>
		</div>
		<div style="height: 50px; background-color:#fff"></div>
	<!--_____________________________FOOTER_____________________________-->	
	<footer class="container-fluid text-center">
		<p>P E P<sup>©</sup></p>
	</footer>
</body>
</html>