<?php
	include "connection.php";
	session_start();
	if (!isset($_SESSION['user'])) 
		header("location:../index.php"); 
?>
<!DOCTYPE html>
<html lang="en">
	<head>
	 	<title>Consultas</title>
	  	<meta charset="utf-8">
	  	<meta name="viewport" content="width=device-width, initial-scale=1">
	  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	  	<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	  	<link rel="stylesheet" type="text/css" href="../css/style.css">
	  	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	  	<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	  	<link rel="icon" href="../img/prontuario.png" type="image/png"/>
	</head>

	<body >

	


		<!-- Modal excluir-->
		<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
		    <div class="modal-dialog" role="document">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
		                <h4 class="modal-title" id="modalLabel">Excluir Item</h4>
		            </div>
		            <div class="modal-body">Deseja realmente excluir este item? </div>
		            <div class="modal-footer">
		                <button type="button" class="btn btn-primary">Sim</button>
		                <button type="button" class="btn btn-default" data-dismiss="modal">N&atilde;o</button>
		            </div>
		        </div>
		    </div>
		</div>
	<!--_______________________NAV____________________-->
			<nav class="navbar navbar-inverse">
				<div class="container-fluid">
					<div class="navbar-header">
					    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>                        
					    </button>
					</div>
					<div class="collapse navbar-collapse" id="myNavbar">
		      			<div class="collapse navbar-collapse" id="myNavbar">
		      			<ul class="nav navbar-nav">
		        			<li class="active"><a href="../php/home.php"></span>Home</a></li>
		    	        	<li class="active"><a href="../php/consultas.php"></span>Consultas</a></li>
		        			<li class="active"><a href="../emManutencao.html"></span>Laudos</a></li>
		        			<li class="active"><a href="../emManutencao.html"></span>Laboratório</a></li>
		        			<li class="active"><a href="../php/cadastro.php"></span>Cadastro</a></li>
		        			<li class="active"><a href="../emManutencao.html"></span>Emergência</a></li>
		      			</ul>
		      			<ul class="nav navbar-nav navbar-right">
					        <li><a href="logout.php"><span class="glyphicon glyphicon-user"></span> Encerrar sessão</a></li>
					    </ul>
				    	<div class="container">    
				    	    <div class="row" >
				    	        <div class="col-md-4 col-md-offset-6">
				    	            <form method="GET" action="" class="search-form">
				    	                <div class="form-group has-feedback">
				    	            		<label for="search" class="sr-only">Search</label>
				    	            		<input type="text" class="form-control" name="search" id="search" placeholder="search">
				    	              		<span class="glyphicon glyphicon-search form-control-feedback"></span>  	
										</div>
				    	            </form>
				    	        </div>
				    	    </div>
				    	</div>
					</nav>

	
	<!--__________________________CONSULTAS______________________-->

	 
	  <div id="main" class="container-fluid">
	       <div id="top" class="row">
	   			<div class="col-md-3">
	   			        <h2>Consultas</h2>
	   			    </div>
	   			 
	   			 
	   			    <div class="col-md-9">
	   			        <button type="button" style="background-color: #62CFEA; border-color: #62CFEA" class="btn btn-primary pull-right h2" class="btn-consulta" onclick="document.getElementById('id01').style.display='block'" style="width:auto;">Marcar consulta</button>
	   			    </div>
	       </div> <!-- /#top -->
	   
	       <hr />
	       <div id="list" class="row">
	       	<div class="table-responsive col-md-12">
	       	       <table class="table table-striped" cellspacing="0" cellpadding="0">
	       	           <thead>
	       	               <tr>
	       	                	<th>DATA</th>
	       	                   	<th>Paciente</th>
	       	                   	<th>Descrição</th>
								<th>Ações</th>
	       	                </tr>
	       	           </thead>
	       	           <tbody>		
						  		<?php


				  			// PESQUISAR O NOME DE ALGUM PACIENTE COM CONSULTA AMRCADA
						  $param = filter_input(INPUT_GET, "search");
						  $sql = "SELECT * FROM consulta INNER JOIN paciente USING(id_paciente) INNER JOIN Pessoa USING(id_pessoa) WHERE status='ATIVO' ORDER BY dt_consulta ASC";
						
						  if(!empty($param)){
							  $sql = "SELECT * FROM consulta INNER JOIN paciente USING(id_paciente) INNER JOIN Pessoa USING(id_pessoa) WHERE status='ATIVO' AND nome LIKE '%". $param . "%'ORDER BY dt_consulta ASC";
						  }
								  $query_consulta_paciente = mysqli_query($connection, $sql);
								  //$test=mysqli_fetch_assoc($query_consulta_paciente);
								while($test=mysqli_fetch_assoc($query_consulta_paciente)){
									$nomePaciente = $test['nome'];
									$descricao = $test['descricao'];
									$dt_consulta = $test['dt_consulta'];
									$id = $test['id_consulta'];

									echo "<tr>";
									echo "<td>$dt_consulta</td>";
	       	                   		echo "<td>$nomePaciente</td>";
	       	                   		echo "<td>$descricao</td>";
									echo "<td class='actions'>
									<a class='btn btn-success btn-xs' href='../emManutencao.html'>Visualizar</a>
									<a class='btn btn-warning btn-xs' href='../emManutencao.html'>Editar</a>
									<a class='btn btn-danger btn-xs'  href='apagar.php?id=". $id . "' data-toggle='modal' data-target=''>Excluir</a>
								</td>";									
									echo "</tr>";
								}
								?>
	       	
	       	           </tbody>
	       	        </table>
	       	
	       	    </div>
	       </div> <!-- /#list -->
	   			
	       <div id="bottom" class="row">
	       		<div class="col-md-12">
	       		         
	       		        <ul class="pagination">
	       		            <li class="disabled"><a>&lt; Anterior</a></li>
	       		            <li class="disabled"><a>1</a></li>
	       		            <li><a href="../emManutencao.html">2</a></li>
	       		            <li><a href="../emManutencao.html">3</a></li>
	       		            <li class="next"><a href="../emManutencao.html" rel="next">Próximo &gt;</a></li>
	       		        </ul><!-- /.pagination -->
	       		 
	       		    </div>
	       </div> <!-- /#bottom -->
	   </div>  <!-- /#main -->


	  <!-- Modal Consulta-->
	  <div id="id01" class="modal">
	    <span onclick="document.getElementById('id01').style.display='none'" 
	  class="close" title="Close Modal">&times;</span>

	 
	    <form class="modal-content animate" method="POST" action="newConsulta.php">
	    <div class="container-02">
	        <label ><b>Nome do paciente:</b></label>
			<select name="nmPaciente[]">
				<option selected>Selecione um paciente</option>
				<?php
					$query_nmPaciente = mysqli_query($connection, "SELECT * FROM paciente INNER JOIN Pessoa ON paciente.id_pessoa = Pessoa.id_pessoa");
					while($test=mysqli_fetch_assoc($query_nmPaciente)){
						$nmPaciente = $test['nome'];
						echo "<option value='$nmPaciente'>$nmPaciente</option>";						
					}
				?>
			</select>		  
			
	        <label ><b>Médico responsável:</b></label>
			<select name="nmMedico[]">
				<option selected>Selecione um medico</option>
				<?php
					$query_nmMedico = mysqli_query($connection, "SELECT * FROM medico INNER JOIN Pessoa ON medico.id_pessoa = Pessoa.id_pessoa");
					while($test2=mysqli_fetch_assoc($query_nmMedico)){
						$nmMedico = $test2['nome'];
						echo "<option value='$nmMedico'>$nmMedico</option>";						
					}
				?>
			</select>

	        <label ><b>Data:</b></label>
	        <input type="date" placeholder="--/--/----" required name="date">

	        <label for="psw"><b>Descrição da consulta*</b></label>
	        <input type="text" placeholder="..." required name="descricao">

	        <button type="submit">Salvar</button>
	      </div>

	      <div class="container-02" style="background-color:#f1f1f1">
	        <button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Cancelar</button>
	      </div>
	    </form>
	</div>
	<!-- -->	
	<div style="height: 190px; background-color:#fff"></div>
	<!--_____________________________FOOTER_____________________________-->	
	<footer class="container-fluid text-center" style="position: relative;
		    width: 100%; bottom: 0">
		<p>P E P<sup>©</sup></p>
	</footer>
</body>
</html>