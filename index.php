<?php
	session_start();
	if(isset($_SESSION['user']))
		header("location:php/home.php");
?>
<!DOCTYPE html>
<html lang="en">
	<head>
	 	<title>Login</title>
	  	<meta charset="utf-8">
	  	<meta name="viewport" content="width=device-width, initial-scale=1">
	  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	  	<link rel="stylesheet" type="text/css" href="css/style.css">
	  	<link rel="icon" href="img/prontuario.png" type="image/png"/>
	</head>

	<body >
	<!--_______________________NAV____________________-->
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
				    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>                        
				    </button>
				</div>
				
			</div>
		</nav>

	<!--___________________________LOGIN___________________________-->
		<form  method="POST" action="php/login.php">
		  <div class="imgcontainer">
		    <img src="img/prontuario.png" alt="Avatar" class="avatar">
		  </div>

		  <div class="container">
		    <label for="uname"><b>Nome de usuário:</b></label>
		    <input type="text" placeholder="Nome de usuário" name="user" required>

		    <label for="psw"><b>Senha:</b></label>
		    <input type="password" placeholder="Senha" name="pass" required>


		    <button type="submit" value = "login" class="btn-login" style="float: right;">Login</button>
		  </div>

		  <div class="container" style="background-color:#f1f1f1">
		    <span class="psw" >Esqueceu a <a href="#">senha?</a></span>
		  </div>
		</form>
	<!--_____________________________FOOTER_____________________________-->	
	<footer class="container-fluid text-center" style="position: absolute;
	    width: 100%; bottom: 0">
			<p>P E P<sup>©</sup></p>
	</footer>
</body>
</html>